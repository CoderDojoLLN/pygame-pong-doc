import pygame

# Initialise Pygame
pygame.init()

# Définit background comme étant une couleur sans rouge, vert et bleu
background = (0, 0, 0)

# Crée une fenêtre de 960 pixels de largeur sur 720 pixels de hauteur
width, height = 960, 720
screen = pygame.display.set_mode((width, height))

# Crée une horloge qui sera utilisée pour limiter le nombre d'images par seconde
clock = pygame.time.Clock()

# Charge le fichier ball.png dans pygame
ball = pygame.image.load("ball.png")
# Crée un rectangle de la taille de l'image
# Le coin du rectangle sera dans le coin de la fenêtre
ballrect = ball.get_rect()

# On commence un boucle infinie
while True:
    # Lorsqu'un évènement de fermeture se produit, le programme est quitté
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

    # Remplit la fenêtre par la couleur background
    screen.fill(background)
    # Copie le contenu de l'image dans le rectangle
    screen.blit(ball, ballrect)
    # Rend visible tout ce qui a été fait
    pygame.display.flip()

    # Limite la vitesse pour ne pas parcourir la boucle plus de 30 fois par seconde
    clock.tick(30)
